<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf8">
    <title>TablaDeMultiplicar</title>
    <link rel="stylesheet" href="practica3tabla.css">
  </head>
  <body>
    <h1><?php echo "Taula de multiplicar" ?> </h1>
    <table border="5">
      <?php
      for ($x = 0; $x <=10; $x++){
        $multiplicacio=5*$x;
        echo "<tr>";
        echo "<th>";
        echo $x;
        echo "<td>";
        echo "5 x " . "$x= " . "$multiplicacio";
        echo "</td>";
        echo "</th>";
        echo "</tr>";
      }
      ?>
    </table>
  <footer>
    <p>Oscar Bravo</p>
  </footer>
  </body>
</html>
