<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf8">
    <title>Formulario</title>
    <link rel="stylesheet" href="formulario.css">
  </head>
  <body>
    <h1><?php echo "Información Personaje" ?> </h1>
    <ul>
      <li>Nombre: <?php echo $_POST['nombre']; ?> </li>
      <li>Sexo: <?php echo $_POST['Sexo']; ?> </li>
      <li>Raza: <?php echo $_POST['raza']; ?> </li>
      <li>Clase: <?php echo $_POST['clase']; ?> </li>
      <li>Armadura: <?php echo $_POST['opcion']; ?> </li>
      <li>Arma: <?php echo $_POST['opcion2']; ?> </li>
    </ul>
    <!-- no admite imagenes de gran tamaño-->
    <?php
    move_uploaded_file($_FILES["archivo"]["tmp_name"],
    "img/" . $_FILES["archivo"]["name"]);
    $imagen = $_FILES["archivo"]["name"];
    echo "<img src='img/$imagen'>";
    ?>
    <footer><?php echo "Oscar Bravo" ?> </footer>
  </body>
</html>
