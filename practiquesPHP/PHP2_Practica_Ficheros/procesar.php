<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Fichero subido</title>
    <link rel="stylesheet" href="practica.css">
  </head>
  <body>
    <header>
      <h1>Procesar fichero</h1>
    </header>
    <div>
    <?php
    $f1=file($_FILES['archivo']["tmp_name"]);
    $f2=fopen('ficheros/fi.txt','w');
    foreach ($f1 as $nLiniea => $linia){
      fwrite($f2,$linia."<br>");
    }
    fwrite($f2,"Texto subido el: ".date("d-m-y H:i:s")."<br><br>");
    fclose($f2);
    $f1=file('ficheros/fi.txt');
    echo "Se ha subido correctamente<br>";
    echo "El contenido del fichero es: <br><br>";
    foreach ($f1 as $nLiniea => $linia){
      echo $linia."<br>";
    }
    ?>
    </div>
    <footer>
      <p>Oscar Bravo Garcia</p>
    </footer>
  </body>
</html>
